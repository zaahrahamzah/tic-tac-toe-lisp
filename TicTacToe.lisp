;this is a tic tac toe game

;displaying the board
(defun display-board (board)
  (dotimes (x 3) ;desc row
    (dotimes (y 3) ;desc column
      (if (= y 2) ;to know if we're at the end of the line or not
          (format t "~A~%"  (aref board x y)) ;aref array represent an obj
          (format t "~A | " (aref board x y) ;~prints literally the var
            ))))
  (format t "~%"))

;update board
(defun update-board (board coords player)
  (setf 
   (aref board (getf coords :x) (getf coords :y))
   player))

;make position
(defun valid-position-p (board coords)
  (equal '- (aref board (getf coords :x) (getf coords :y))))

;cpu player
(defun cpu-turn (board)
  (let* ((x (random (array-dimension board 0) (make-random-state t)))
         (y (random (array-dimension board 0) (make-random-state t)))
         (coords `(:x ,x :y ,y)))
    (if (valid-position-p board coords)
        coords
      (cpu-turn board))))

;human player
(defun player-turn (board)
  (format t "Please enter coordinate (x enter y): ")
  
  ;so that machine wont stop so allowed junk
  (let ((x (parse-integer (read-line) :junk-allowed t)))
    (unless (member x '(0 1 2))
      (player-turn board))
    
    (let ((y (parse-integer (read-line) :junk-allowed t)))
      (unless (member y '(0 1 2))
      (player-turn board))
    
      (let ((coords `(:x ,x :y ,y)))
        (if (valid-position-p board coords)
            coords
          (player-turn board))))))      
         
;goal outcome
(defun game-over-p (board)
  (flet ((draw-p (board)
                 (let ((counter 0))
                   (dotimes (x 3)
                     (dotimes (y 3)
                       (when (equal '- (aref board x y))
                         (incf counter))))
                   
                   (= 0 counter))))
    
    (cond
     ;rows
     ((and (equal (aref board 0 0) (aref board 0 1)) (equal (aref board 0 0) (aref board 0 2)) (not (equal (aref board 0 0) '-))) t)
     ((and (equal (aref board 1 0) (aref board 1 1)) (equal (aref board 1 0) (aref board 1 2)) (not (equal (aref board 1 0) '-))) t)
     ((and (equal (aref board 2 0) (aref board 2 1)) (equal (aref board 2 0) (aref board 2 2)) (not (equal (aref board 2 0) '-))) t)

     ;columns
     ((and (equal (aref board 0 0) (aref board 1 0)) (equal (aref board 0 0) (aref board 2 0)) (not (equal (aref board 0 0) '-))) t)
     ((and (equal (aref board 0 1) (aref board 1 1)) (equal (aref board 0 1) (aref board 2 1)) (not (equal (aref board 0 1) '-))) t)
     ((and (equal (aref board 0 2) (aref board 1 2)) (equal (aref board 0 2) (aref board 2 2)) (not (equal (aref board 0 2) '-))) t)
     
     ;diagonals
     ((and (equal (aref board 0 0) (aref board 1 1)) (equal (aref board 0 0) (aref board 2 2)) (not (equal (aref board 0 0) '-))) t)
     ((and (equal (aref board 0 2) (aref board 1 1)) (equal (aref board 0 2) (aref board 2 0)) (not (equal (aref board 0 2) '-))) t)

     ;draw
     ((draw-p board) t)
     
     ;otherwise
      (t nil))))



;main
(defun game (&key (board (make-array '(3 3) :initial-element '-)))
  
  ;(update-board board '(:x 0 :y 0) "x")
  
  (let ((turn-counter (1+ (random 2 (make-random-state t)))))
    (do () 
        ((game-over-p board))
      
      (display-board board)
      
      ;deciding who starts first
      (if (evenp turn-counter)
          (let ((coords (player-turn board)))
            (update-board board coords "x"))
        
          (let ((coords (cpu-turn board)))
            (update-board board coords "o")))
      
      (incf turn-counter))
    
 
  (display-board board)
  (format t "Game Over!~%")))
  
(game)

